import numpy as np
import scipy.constants as PC
import matplotlib.pyplot as plt

class Dielectric:
    def __init__(self, epsilon, losstan, thickness, error, number_layers):
        self.epsilon = epsilon
        self.losstan = losstan
        self.thickness = thickness
        self.error = error
        self.number_layers = number_layers
     
    def get_eps():
        return self.epsilon

    def get_losstan():
        return self.losstan

    def get_thickness():
        return self

# use curve_fit instead of leastsq. We don't need place holder variables with curve_fit.
class Transmittance:
    def __init__(self):
        self.dielectric_layers = []
        self.indices = []
        self.refl_coef = []
        self.X_matrix = []
        self.D_matrix = []
        self.M_matrix = []
    
    # each dielectric_layer contains dielectric class for all layers.
    def add_layers(self, *args):
        for i in args:
            self.dielectric_layers.append(i)
        return self.dielectric_layers
        
    def refract_index(self):
        for i in self.dielectric_layers:
            self.indices.append(i.get_eps())
        return self.indices

    def reflect_coef(self):
        n = self.indices
        for i in range(len(n)-1):
            self.refl_coef.append((n[i]-n[i+1])/(n[i]+n[i+1]))
        return self.refl_coef

    def calcX(self, freq_hz):
        n = self.indices
        for i in range(len(n)):
            x_tmp = np.exp(1.j*(2*np.pi*freq_hz*self.dielectric_layers[i].thickness*n[i]))
            self.X_matrix.append(x_tmp)
        return self.X_matrix

    def calcD(self, freq_hz):
        refl = reflect_coef(self)
        X = calcX(self, freq_hz)
        self.D_matrix.append([[1, refl[0]],[refl[0],1]])
        for i in range(len(X)):
            d = np.dot(np.array([[X[i], 0], [0, X[i]**-1]]), np.array([[1,refl[i+1]],[refl[i+1],1]]))
            self.D.append(d)
        return self.D_matrix, refl

    def calcM(self, freq_hz):
        D, refl = calcD(self, freq_hz)
        for i in range(len(D)):
            m = (1+refl[i])**-1*D[i]
            self.M_matrix.append(m)
        return self.M_matrix

    def tx(self, freq_hz):
        M = calcM(self, freq_hz)
        M_0 = M[0]
        for m in M[1:]:
            M_0 = np.dot(M_0, m)
        dtx = abs((M_0[0,0])**-1)**2
        return dtx
        



        
